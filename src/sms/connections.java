package sms;
import  java.sql.*;
import com.google.gson.Gson;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;


//All connection are done here
class connections {
    public void databaseConnection(String dburl, String username, String pswd, String statement){
        try(


                Connection conn = DriverManager.getConnection(
                        dburl, username,pswd);

                Statement stmt = conn.createStatement();

        ){

            String strSelect = statement;


            ResultSet rset = stmt.executeQuery(strSelect);


            while(rset.next()){
                long number = rset.getLong("number");
                String msg = rset.getString("Message");
                SMS payload = new SMS(number,msg);
                Gson gson = new Gson();
                String loader = gson.toJson(payload);
                payload.sendPOST(loader,"POST","MOZILLA");


            }
            conn.close();


        }catch(SQLException ex){
            ex.printStackTrace();

        } catch (IOException ex) {
            Logger.getLogger(SMS.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void sendPOST(String payload,String POST_URL, String USER_AGENT) throws IOException {
        URL obj = new URL(POST_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type","application/json");



        // For POST only - START
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(payload.getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        //System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
        } else {
            System.out.println("POST request not worked");
        }
    }
}
