package voidPackage;

import java.sql.*; // Use 'Connection', 'Statement' and 'ResultSet' classes in java.sql package
import com.google.gson.Gson;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SMS {

    private String DATABASE;
    private String POST_URL;
    private String username;
    private String password;
    private String api_key;
    private long to;


    SMS(long number, String message) {
        long num = number;
        String text = message;
        String type = "text";

    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public void setDATABASE(String DATABASE) {
        this.DATABASE = DATABASE;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPOST_URL(String POST_URL) {
        this.POST_URL = POST_URL;
    }

    public void setTo(long to) {
        this.to = to;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //Connect the application to the database
    /**
     * Param: url : database connection;
     *         user: username for database
     *         pswd: password for database
     *        statement: SQL statemt in string format
     *
     * **/
    public void databaseConnection(String dburl, String username, String pswd, String statement){
        try(


                Connection conn = DriverManager.getConnection(
                        dburl, username,pswd);

                Statement stmt = conn.createStatement();

        ){

            String strSelect = statement;


            ResultSet rset = stmt.executeQuery(strSelect);


            while(rset.next()){
                long number = rset.getLong("number");
                String msg = rset.getString("Message");
                SMS payload = new SMS(number,msg);
                Gson gson = new Gson();
                String loader = gson.toJson(payload);
                payload.sendPOST(loader,"POST","MOZILLA");


            }
            conn.close();


        }catch(SQLException ex){
            ex.printStackTrace();

        } catch (IOException ex) {
            Logger.getLogger(SMS.class.getName()).log(Level.SEVERE, null, ex);

        }
    }


    public void sendPOST(String payload,String POST_URL, String USER_AGENT) throws IOException {
        URL obj = new URL(POST_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type","application/json");



        // For POST only - START
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(payload.getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        //System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
        } else {
            System.out.println("POST request not worked");
        }
    }
}






